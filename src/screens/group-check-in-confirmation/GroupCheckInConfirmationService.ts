import { format } from "date-fns";
import { executeQuery } from "../../aws/db/dbOperation";
import { getCurrentJapanTime } from "../../environments/TimeUtils";
// get Data from gender
export const fetchGenderName = async (genderCode: string) => {
  const method = "POST";
  const queryString =
    "SELECT name FROM gender WHERE gender_code ='" + genderCode + "';";
  return executeQuery(method, queryString);
};

//insert reception FamilyOrderNumber==0
export const insertReceptionData = async (
  cityCode: string,
  eventId: number,
  venueId: number,
  memberId: number,
  modifierId: string,
  lgapId: string,
  userRank: string,
  lastName: string,
  firstName: string,
  lastNameKana: string,
  firstNameKana: string,
  dateOfBirth: Date | null,
  genderCode: string,
  postalCode: string,
  address: string,
  relationship: string,
  receptionTypeCode: string,
  familyOrderNumber: number
) => {
  const currentJapanTime = getCurrentJapanTime();
  let formattedDateOfBirth: string;
  if (dateOfBirth) {
    formattedDateOfBirth = format(dateOfBirth, "yyyy-MM-dd");
  } else {
    formattedDateOfBirth = "";
  }

  const method = "POST";

  const queryString = `INSERT INTO reception (
      city_code, event_id, venue_id,member_id, history_number, accepted_terminal_id, accepted_timestamp, modifier_id, modification_timestamp, is_deleted, lgap_id, user_rank, lastname, firstname, lastname_kana, firstname_kana, date_of_birth, gender_code, postal_code, address, relationship, reception_type_code, family_order_number
  )
  VALUES (
      '${cityCode}',
      ${eventId},
      ${venueId}, 
      0, 
      0,
      NULL,
      '${currentJapanTime}', 
      '${modifierId}', 
      '${currentJapanTime}', 
      false,
      CASE
        WHEN '${lgapId}' ='' THEN NULL
        ELSE '${lgapId}'
      END,
      CASE
      WHEN '${userRank}' ='' THEN NULL
      ELSE '${userRank}'
    END,
      '${lastName}',
      '${firstName}', 
      '${lastNameKana}',
      '${firstNameKana}',
      '${formattedDateOfBirth}',
      '${genderCode}',
      '${postalCode}', 
      '${address}', 
       '本人', 
      '${receptionTypeCode}', 
      ${familyOrderNumber})
    RETURNING reception_id;`;

  return executeQuery(method, queryString);
};

//insert reception family FamilyOrderNumber != 0
export const insertReceptionDataFamily = async (
  cityCode: string,
  eventId: number,
  venueId: number,
  receptionId: number,
  memberId: number,
  modifierId: string,
  lgapId: string,
  userRank: string,
  lastName: string,
  firstName: string,
  lastNameKana: string,
  firstNameKana: string,
  dateOfBirth: Date | null,
  genderCode: string,
  postalCode: string,
  address: string,
  relationship: string,
  receptionTypeCode: string,
  familyOrderNumber: number
) => {
  const currentJapanTime = getCurrentJapanTime();

  let formattedDateOfBirth: string;
  if (dateOfBirth) {
    formattedDateOfBirth = format(dateOfBirth, "yyyy-MM-dd");
  } else {
    formattedDateOfBirth = "";
  }

  const method = "POST";

  const queryString = `INSERT INTO reception (
          city_code, event_id, venue_id, reception_id, member_id, history_number, accepted_terminal_id, accepted_timestamp, modifier_id, modification_timestamp, is_deleted, lgap_id, user_rank, lastname, firstname, lastname_kana, firstname_kana, date_of_birth, gender_code, postal_code, address, relationship, reception_type_code, family_order_number
      )
      OVERRIDING SYSTEM VALUE
      VALUES (
          '${cityCode}',
          ${eventId},
          ${venueId},
          ${receptionId},
          ${familyOrderNumber},
          0,
          NULL,
          '${currentJapanTime}',
          '${modifierId}',
          '${currentJapanTime}',
          false,
          NULL,
          NULL,
          '${lastName}',
          '${firstName}',
          '${lastNameKana}',
          '${firstNameKana}',
          '${formattedDateOfBirth}',
          '${genderCode}',
          '${postalCode}',
          '${address}',
          '${relationship}',
          '${receptionTypeCode}',
          ${familyOrderNumber})`;

  return executeQuery(method, queryString);
};
